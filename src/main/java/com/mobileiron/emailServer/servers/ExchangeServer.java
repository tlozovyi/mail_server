package com.mobileiron.emailServer.servers;

import com.mobileiron.emailServer.CustomExchangeService;
import com.mobileiron.emailServer.pojo.Contact;
import com.mobileiron.emailServer.pojo.Event;
import com.mobileiron.emailServer.pojo.Mail;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.*;
import microsoft.exchange.webservices.data.core.enumeration.property.time.DayOfTheWeek;
import microsoft.exchange.webservices.data.core.enumeration.search.FolderTraversal;
import microsoft.exchange.webservices.data.core.enumeration.service.DeleteMode;
import microsoft.exchange.webservices.data.core.enumeration.service.SendInvitationsMode;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.FolderSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.EmailAddress;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import microsoft.exchange.webservices.data.property.complex.PhysicalAddressEntry;
import microsoft.exchange.webservices.data.property.complex.recurrence.pattern.Recurrence;
import microsoft.exchange.webservices.data.search.FindFoldersResults;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.FolderView;
import microsoft.exchange.webservices.data.search.ItemView;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

public class ExchangeServer extends ServerConnection {
    private ExchangeService service;

    public ExchangeServer(String serverAddress, String user, String password) {
        try {
            ExchangeCredentials credentials = new WebCredentials(user, password);
            service = new CustomExchangeService().createConnection(serverAddress, credentials);
            service.setCredentials(credentials);
            service.setUrl(new URI(serverAddress));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    //Mails

    public void createMail(Mail mail) {
        System.out.println("========== Create mail ==========");
        try {
            EmailMessage msg = new EmailMessage(service);
            if (mail.getSubject() != null) {
                System.out.println("Subject: " + mail.getSubject());
                msg.setSubject(mail.getSubject());
            }
            if (!mail.getTo().isEmpty()) {
                System.out.println("To: " + mail.getTo());
                for (String to : mail.getTo()) {
                    msg.getToRecipients().add(to);
                }
            }
            if (mail.getCc() != null) {
                System.out.println("Cc: " + mail.getCc());
                msg.getCcRecipients().add(mail.getCc());
            }
            if (mail.getBcc() != null) {
                System.out.println("Bcc: " + mail.getBcc());
                msg.getBccRecipients().add(mail.getBcc());
            }
            if (mail.getMessage() != null) {
                System.out.println("Message: " + mail.getMessage());
                msg.setBody(MessageBody.getMessageBodyFromText(mail.getMessage()));
            }
            if (mail.getAttachment() != null) {
                System.out.println("Attachment: " + mail.getAttachment());
                msg.getAttachments().addFileAttachment(mail.getAttachment());
            }

            msg.send();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isMailPresentInInbox(String mailSubject) {
        System.out.println("Is mail present in inbox: " + mailSubject);
        ItemView view = new ItemView(50);
        FindItemsResults<Item> findResults;
        try {
            do {
                findResults = service.findItems(WellKnownFolderName.Inbox, view);
                for (Item item : findResults.getItems()) {
                    System.out.println("Found email with subject: " + item.getSubject());
                    if (item.getSubject().equals(mailSubject)) {
                        return true;
                    }
                }
                view.setOffset(view.getOffset() + 50);
            } while (findResults.isMoreAvailable());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Needed mail not found");
        return false;
    }

    public boolean isMailPresentInFolder(String mailSubject, String folderName) {
        System.out.println("Is mail '" + mailSubject + "' present in folder: " + folderName);
        try {
            FolderView folderView = new FolderView(50);
            folderView.setPropertySet(new PropertySet(BasePropertySet.IdOnly));
            folderView.getPropertySet().add(FolderSchema.DisplayName);
            folderView.setTraversal(FolderTraversal.Deep);

            FindFoldersResults findFolderResults;
            do {
                findFolderResults = service.findFolders(WellKnownFolderName.Inbox, folderView);
                for (Folder f : findFolderResults.getFolders()) {
//                    System.out.println("Found folder: " + f.getDisplayName());
                    if (f.getDisplayName().equals(folderName) && isMailPresentInFolder(mailSubject, f.getId())) {
                        return true;
                    }
                }
                folderView.setOffset(folderView.getOffset() + 50);
            } while (findFolderResults.isMoreAvailable());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Needed mail not found");
        return false;
    }

    public void deleteMailInInbox(String mailSubject) {
        System.out.println("Delete mail: " + mailSubject);
        ItemView view = new ItemView(50);
        FindItemsResults<Item> findResults;
        try {
            do {
                findResults = service.findItems(WellKnownFolderName.Inbox, view);
                for (Item item : findResults.getItems()) {
                    if (item.getSubject().equals(mailSubject)) {
                        item.delete(DeleteMode.HardDelete);
                        System.out.println("Mail deleted");
                    }
                }
                view.setOffset(view.getOffset() + 50);
            } while (findResults.isMoreAvailable());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAllMailsInInbox() {
        System.out.println("Delete all mails in Inbox");
        ItemView view = new ItemView(50);
        FindItemsResults<Item> findResults;
        try {
            do {
                findResults = service.findItems(WellKnownFolderName.Inbox, view);
                for (Item item : findResults.getItems()) {
                    System.out.println("Deleting mail: " + item.getSubject());
                    item.delete(DeleteMode.HardDelete);
                }
                view.setOffset(view.getOffset() + 50);
            } while (findResults.isMoreAvailable());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("All mails in Inbox are deleted");
    }

    public void deleteAllMailsInFolder(String folderName) {
        System.out.println("Delete all mails in folder: " + folderName);
        try {
            FolderView folderView = new FolderView(50);
            folderView.setPropertySet(new PropertySet(BasePropertySet.IdOnly));
            folderView.getPropertySet().add(FolderSchema.DisplayName);
            folderView.setTraversal(FolderTraversal.Deep);

            FindFoldersResults findFolderResults;
            do {
                findFolderResults = service.findFolders(WellKnownFolderName.Inbox, folderView);
                for (Folder f : findFolderResults.getFolders()) {
                    deleteAllMailsInFolder(f.getId());
                }
                folderView.setOffset(folderView.getOffset() + 50);
            } while (findFolderResults.isMoreAvailable());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("All mails in folder are deleted");
    }

    private void deleteAllMailsInFolder(FolderId folderId) throws Exception {
        ItemView view = new ItemView(50);
        FindItemsResults<Item> findResults;
        do {
            findResults = service.findItems(folderId, view);
            for (Item item : findResults.getItems()) {
                if (item instanceof EmailMessage) {
                    System.out.println("Deleting mail: " + item.getSubject());
                    item.delete(DeleteMode.HardDelete);
                }
            }
            view.setOffset(view.getOffset() + 50);
        } while (findResults.isMoreAvailable());
    }

    private boolean isMailPresentInFolder(String mailSubject, FolderId folderId) throws Exception {
        ItemView view = new ItemView(5);
        FindItemsResults<Item> findResults;
        do {
            findResults = service.findItems(folderId, view);
            for (Item item : findResults.getItems()) {
                System.out.println("Found email with subject: " + item.getSubject());
                if (item.getSubject().equals(mailSubject)) {
                    return true;
                }
            }
            view.setOffset(view.getOffset() + 5);
        } while (findResults.isMoreAvailable());
        return false;
    }

    //Contacts

    @Override
    public void createContact(Contact contact) {
        System.out.println("========== Create contact ==========");
        try {
            microsoft.exchange.webservices.data.core.service.item.Contact serverContact = new microsoft.exchange.webservices.data.core.service.item.Contact(service);

            if (contact.getFirstName() != null) {
                System.out.println("First name: " + contact.getFirstName());
                serverContact.setGivenName(contact.getFirstName());
            }

            if (contact.getLastName() != null) {
                System.out.println("Last name: " + contact.getLastName());
                serverContact.setSurname(contact.getLastName());
            }

            if (contact.getMiddleName() != null) {
                System.out.println("Middle name: " + contact.getMiddleName());
                serverContact.setMiddleName(contact.getMiddleName());
            }

            if (contact.getPhoneNumber() != null) {
                System.out.println("Phone: " + contact.getPhoneNumber());
                serverContact.getPhoneNumbers().setPhoneNumber(PhoneNumberKey.MobilePhone, contact.getPhoneNumber());
            }

            if (contact.getEmail() != null) {
                System.out.println("Email: " + contact.getEmail());
                serverContact.getEmailAddresses().setEmailAddress(EmailAddressKey.EmailAddress1, new EmailAddress(contact.getEmail()));
            }

            if (contact.getAddress() != null) {
                System.out.println("Address: " + contact.getAddress());
                PhysicalAddressEntry street = new PhysicalAddressEntry();
                street.setStreet(contact.getAddress());
                serverContact.getPhysicalAddresses().setPhysicalAddress(PhysicalAddressKey.Home, street);
            }

            if (contact.getOrganization() != null) {
                System.out.println("Organization: " + contact.getOrganization());
                serverContact.setCompanyName(contact.getOrganization());
            }

            if (contact.getNickname() != null) {
                System.out.println("Nickname: " + contact.getNickname());
                serverContact.setNickName(contact.getNickname());
            }

            if (contact.getWebsite() != null) {
                System.out.println("Website: " + contact.getWebsite());
                serverContact.setBusinessHomePage(contact.getWebsite());
            }

            serverContact.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAllContacts() {
        System.out.println("========== Delete all contacts ==========");
        ItemView view = new ItemView(50);
        FindItemsResults<Item> findResults;
        try {
            do {
                findResults = service.findItems(WellKnownFolderName.Contacts, view);
                for (Item item : findResults.getItems()) {
                    microsoft.exchange.webservices.data.core.service.item.Contact contact = (microsoft.exchange.webservices.data.core.service.item.Contact) item;
                    System.out.println("Deleting contacts: " + contact.getGivenName() + " " + contact.getSurname());
                    item.delete(DeleteMode.HardDelete);
                }
                view.setOffset(view.getOffset() + 50);
            } while (findResults.isMoreAvailable());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("All contacts are deleted");
    }

    @Override
    public boolean isContactPresent(String firstName, String lastName) {
        System.out.println("Is contact present: " + firstName + " " + lastName);
        ItemView view = new ItemView(50);
        FindItemsResults<Item> findResults;
        try {
            do {
                findResults = service.findItems(WellKnownFolderName.Contacts, view);
                for (Item item : findResults.getItems()) {
                    microsoft.exchange.webservices.data.core.service.item.Contact contact = (microsoft.exchange.webservices.data.core.service.item.Contact) item;
                    System.out.println("Found contact: " + contact.getGivenName() + " " + contact.getSurname());

                    if (firstName != null && lastName != null
                            && firstName.equals(contact.getGivenName()) && lastName.equals(contact.getSurname())) {
                        System.out.println("found");
                        return true;
                    }

                    if (firstName != null && firstName.equals(contact.getGivenName())) {
                        return true;
                    }

                    if (lastName != null && lastName.equals(contact.getSurname())) {
                        return true;
                    }
                }
                view.setOffset(view.getOffset() + 50);
            } while (findResults.isMoreAvailable());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Needed contact not found");
        return false;
    }

    //Event

    @Override
    public void createEvent(Event event) {
        System.out.println("========== Create event ==========");
        try {
            Appointment appointment = new Appointment(service);

            if (event.getTitle() != null) {
                System.out.println("Title: " + event.getTitle());
                appointment.setSubject(event.getTitle());
            }

            if (event.getLocation() != null) {
                System.out.println("Location: " + event.getLocation());
                appointment.setLocation(event.getLocation());
            }

            if (event.getStartDate() != null) {
                System.out.println("Start date: " + event.getStartDate());
                appointment.setStart(event.getStartDate().getTime());
            }

            if (event.getEndDate() != null) {
                System.out.println("End date: " + event.getEndDate());
                appointment.setEnd(event.getEndDate().getTime());
            }

            if (event.getAllDay() != null) {
                System.out.println("All day: " + event.getAllDay());
                appointment.setIsAllDayEvent(event.getAllDay());
            }

            if (event.getRecurrence() != Event.Recurrence.NONE) {
                System.out.println("Recurrence: " + event.getRecurrence());
                Date startDate = event.getStartDate() != null ? event.getStartDate().getTime() : new Date();
                switch (event.getRecurrence()) {
                    case EVERY_DAY:
                        appointment.setRecurrence(new Recurrence.DailyPattern());
                        break;
                    case EVERY_3_DAYS:
                        appointment.setRecurrence(new Recurrence.DailyPattern(startDate, 3));
                        break;
                    case WEEKDAYS:
                        appointment.setRecurrence(new Recurrence.WeeklyPattern(startDate, 1,
                                DayOfTheWeek.Monday, DayOfTheWeek.Tuesday, DayOfTheWeek.Wednesday, DayOfTheWeek.Thursday, DayOfTheWeek.Friday));
                        break;
                    case MONTHLY:
                        appointment.setRecurrence(new Recurrence.MonthlyPattern());
                        break;
                }
            }

            if (event.getRecurrence() != Event.Recurrence.NONE && event.getUntilDate() != null) {
                System.out.println("Until date: " + event.getUntilDate());
                appointment.getRecurrence().setEndDate(event.getUntilDate().getTime());
            }

            if (event.getAllDay() != null) {
                System.out.println("All day: " + event.getAllDay());
                appointment.setIsAllDayEvent(event.getAllDay());
            }

            if (!event.getGuestMails().isEmpty()) {
                System.out.println("Guests: " + event.getGuestMails());
                for (String attendee : event.getGuestMails())
                appointment.getRequiredAttendees().add(attendee);
            }

            if (event.getDescription() != null) {
                System.out.println("Description: " + event.getDescription());
                appointment.setBody(MessageBody.getMessageBodyFromText(event.getDescription()));
            }

            if (event.getGuestMails().isEmpty()) {
                appointment.save();
            } else {
                appointment.save(SendInvitationsMode.SendOnlyToAll);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
