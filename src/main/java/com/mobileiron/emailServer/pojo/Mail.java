package com.mobileiron.emailServer.pojo;

import java.util.ArrayList;

public class Mail {
    private ArrayList<String> toList = new ArrayList<String>();
    private String cc;
    private String bcc;
    private String subject;
    private String message;
    private String attachment;  //full path to file

    public ArrayList<String> getTo() {
        return toList;
    }

    public Mail addTo(String to) {
        toList.add(to);
        return this;
    }

    public String getCc() {
        return cc;
    }

    public Mail setCc(String cc) {
        this.cc = cc;
        return this;
    }

    public String getBcc() {
        return bcc;
    }

    public Mail setBcc(String bcc) {
        this.bcc = bcc;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Mail setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Mail setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getAttachment() {
        return attachment;
    }

    public Mail setAttachment(String attachment) {
        this.attachment = attachment;
        return this;
    }
}
