package com.mobileiron.emailServer.servers;


import com.mobileiron.emailServer.pojo.Contact;
import com.mobileiron.emailServer.pojo.Event;
import com.mobileiron.emailServer.pojo.Mail;

public abstract class ServerConnection {
    //Mails
    public abstract void createMail(Mail mail);
    public abstract boolean isMailPresentInInbox(String mailSubject);
    public abstract boolean isMailPresentInFolder(String mailSubject, String folderName);
    public abstract void deleteMailInInbox(String mailSubject);
    public abstract void deleteAllMailsInInbox();
    public abstract void deleteAllMailsInFolder(String folderName);

    //Contacts
    public abstract void createContact(Contact contact);
    public abstract void deleteAllContacts();
    public abstract boolean isContactPresent(String firstName, String lastName);

    //Events
    public abstract void createEvent(Event event);
}
