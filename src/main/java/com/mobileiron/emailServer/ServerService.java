package com.mobileiron.emailServer;


import com.mobileiron.emailServer.servers.ExchangeServer;
import com.mobileiron.emailServer.servers.ServerConnection;

public class ServerService {
    private ServerConnection serverConnection;

    public ServerService(ServerType serverType, String username, String password) {
        switch (serverType) {
            case EXCHANGE_2010_SP2:
            case EXCHANGE_2013:
            case OFFICE_365:
                serverConnection = new ExchangeServer(serverType.getAddress(), username, password);
                break;
        }
    }

    public ServerConnection getServerConnection() {
        return serverConnection;
    }
}
