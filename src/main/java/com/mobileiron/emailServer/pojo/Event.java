package com.mobileiron.emailServer.pojo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Event {
    public String title;
    public String location;
    public Calendar startDate;
    public Calendar endDate;
    public Calendar untilDate;
    public Boolean isAllDay;
    public List<String> guestMails = new ArrayList<String>();

    public String description;
    public Recurrence recurrence = Recurrence.NONE;
    public String reminder; //local usage only

    public Event setTitle(String title) {
        this.title = title;
        return this;
    }

    public Event setLocation(String location) {
        this.location = location;
        return this;
    }

    public Event setAllDay(boolean allDay) {
        isAllDay = allDay;
        return this;
    }

    public Event addGuestMails(String guestMail) {
        guestMails.add(guestMail);
        return this;
    }

    public Event setDescription(String description) {
        this.description = description;
        return this;
    }

    public Event setReminder(String reminder) {
        this.reminder = reminder;
        return this;
    }

    public Event setStartDate(Calendar startDate) {
        this.startDate = startDate;
        return this;
    }

    public Event setEndDate(Calendar endDate) {
        this.endDate = endDate;
        return this;
    }

    public Event setRecurrence(Recurrence recurrence) {
        this.recurrence = recurrence;
        return this;
    }

    public Event setUntilDate(Calendar untilDate) {
        this.untilDate = untilDate;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public Calendar getUntilDate() {
        return untilDate;
    }

    public Boolean getAllDay() {
        return isAllDay;
    }

    public List<String> getGuestMails() {
        return guestMails;
    }

    public String getDescription() {
        return description;
    }

    public Recurrence getRecurrence() {
        return recurrence;
    }

    public String getReminder() {
        return reminder;
    }

    public enum Recurrence {
        NONE, EVERY_DAY, EVERY_3_DAYS, WEEKDAYS, MONTHLY
    }
}
