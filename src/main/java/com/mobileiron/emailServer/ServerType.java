package com.mobileiron.emailServer;

public enum ServerType {
    EXCHANGE_2010_SP2("https://ex2010sp2.auto7.mobileiron.com/ews/Exchange.asmx"),
    EXCHANGE_2013("https://ex2013.auto19.mobileiron.com/ews/Exchange.asmx"),
    OFFICE_365("https://outlook.office365.com/ews/Exchange.asmx");

    private String address;

    ServerType(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
