package com.mobileiron.emailServer;

import microsoft.exchange.webservices.data.EWSConstants;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.cert.X509Certificate;

public class CustomExchangeService extends ExchangeService {

    @Override
    protected Registry<ConnectionSocketFactory> createConnectionSocketFactoryRegistry() {
        SSLContext ctx;
        try {
            ctx = SSLContext.getInstance("SSL");
            ctx.init(null, getTrustManagers(), null);
        } catch (Exception e) {
            System.out.println("Exception loading Bold trust store");
            throw new RuntimeException(e);
        }

        SSLConnectionSocketFactory sslFactory = new SSLConnectionSocketFactory(ctx, NoopHostnameVerifier.INSTANCE);


        return RegistryBuilder.<ConnectionSocketFactory>create()
                .register(EWSConstants.HTTP_SCHEME, new PlainConnectionSocketFactory())
                .register(EWSConstants.HTTPS_SCHEME, sslFactory)
                .build();
    }

    public CustomExchangeService createConnection(String url, ExchangeCredentials credentials){
        CustomExchangeService customExchangeService = new CustomExchangeService();
        customExchangeService.setCredentials(credentials);
        try {
            customExchangeService.setUrl(new URI(url));
        } catch (URISyntaxException e) {
            e.printStackTrace();

        }
        return customExchangeService;
    }

    private TrustManager[ ] getTrustManagers() {
        TrustManager[ ] certs = new TrustManager[ ] {
                new X509TrustManager() {
                    public X509Certificate[ ] getAcceptedIssuers() { return null; }
                    public void checkClientTrusted(X509Certificate[ ] certs, String t) { }
                    public void checkServerTrusted(X509Certificate[ ] certs, String t) { }
                }
        };
        return certs;
    }

}
